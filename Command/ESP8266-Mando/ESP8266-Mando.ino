#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#define IP ""

const char* ssid     = "DAM";
const char* password = "";
uint16_t tx = 45456;

#define _PLAYER_ID 1

#define _INPUT_START 0
#define _INPUT_SELECT 4
#define _INPUT_UP 13
#define _INPUT_DOWN 12
#define _INPUT_LEFT 14
#define _INPUT_RIGHT 5
#define _INPUT_B 2
#define _INPUT_A 16


#define UP 1
#define DOWN 2
#define RIGHT 3
#define LEFT 4
#define SELECT 5
#define START 6
#define A 7
#define B 8

bool state[9];

void setup() {
  Serial.begin(115200);
  Serial.println(_INPUT_START);
  Serial.println(", ");
  Serial.println(_INPUT_SELECT);
  Serial.println(", ");
  Serial.println(_INPUT_UP);
  Serial.println(", ");
  Serial.println(_INPUT_DOWN);
  Serial.println(", ");
  Serial.println(_INPUT_LEFT);
  Serial.println(", ");
  Serial.println(_INPUT_RIGHT);
  Serial.println(", ");
  Serial.println(_INPUT_B);
  Serial.println(", ");
  Serial.println(_INPUT_A);
  Serial.println(", ");
  pinMode(_INPUT_START,INPUT);        // Start
  pinMode(_INPUT_SELECT,INPUT);       // Select
  pinMode(_INPUT_UP,INPUT);           // Up
  pinMode(_INPUT_DOWN,INPUT);         // Down
  pinMode(_INPUT_LEFT,INPUT);         // Left
  pinMode(_INPUT_RIGHT,INPUT);        // Right
  pinMode(_INPUT_B,INPUT);            // B
  pinMode(_INPUT_A,INPUT);            // A
  connect();
  loadStates();
  Serial.print("read");
  TCPServer();

}
#define START 6
#define SELECT 5
#define UP 1
#define DOWN 2
#define LEFT 4
#define RIGHT 3
#define B 8
#define A 7
void loadStates(){
  state[UP] = !digitalRead(_INPUT_UP);
  state[DOWN] = !digitalRead(_INPUT_DOWN);
  state[RIGHT] = !digitalRead(_INPUT_RIGHT);
  state[LEFT] = !digitalRead(_INPUT_LEFT);
  state[SELECT] = !digitalRead(_INPUT_SELECT);
  state[START] = !digitalRead(_INPUT_START);
  state[A] = !digitalRead(_INPUT_A);
  state[B] = !digitalRead(_INPUT_B);
}

WiFiClient client;
void TCPServer () {
  const int httpPort = tx;
  if (!client.connect(IP, httpPort)) {
    Serial.println("connection failed");
  }
}

void connect(){
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  client.print("10");

}

long temps = millis();

void vSendTCP(int in, int data){
  String user = String(in);
  String dat = String(data);
  String dat2 = String(!state[data]);
  client.print(user + dat + dat2);
  //Serial.println(user + dat);
  state[data] = !state[data];
}

void printStatus(){
  if(!digitalRead(_INPUT_START) != state[START]){
    Serial.println("Start selected.");
    //vSendUDP(START,1);
    vSendTCP(_PLAYER_ID,START);
  }
  if(!digitalRead(_INPUT_SELECT) != state[SELECT]){
    Serial.println("Select selected.");
    //vSendUDP(SELECT,1);
    vSendTCP(_PLAYER_ID,SELECT);
  }
  if(!digitalRead(_INPUT_UP) != state[UP]){
    Serial.println("UP selected.");
    //vSendUDP(UP,1);
    vSendTCP(_PLAYER_ID,UP);
  }
  if(!digitalRead(_INPUT_DOWN) != state[DOWN]){
    Serial.println("Down selected.");
    //vSendUDP(DOWN,1);
    vSendTCP(_PLAYER_ID,DOWN);
  }
  if(!digitalRead(_INPUT_LEFT) != state[LEFT]){
    Serial.println("Left selected.");
    //vSendUDP(LEFT,1);
    vSendTCP(_PLAYER_ID,LEFT);
  }
  if(!digitalRead(_INPUT_RIGHT) != state[RIGHT]){
    Serial.println("Right selected.");
    //vSendUDP(RIGHT,1);
    vSendTCP(_PLAYER_ID,RIGHT);
  }
  if(!digitalRead(_INPUT_B) != state[B]){
    Serial.println("B selected.");
    //vSendUDP(B,1);
    vSendTCP(_PLAYER_ID,B);
  }
  if(!digitalRead(_INPUT_A) != state[A]){
    Serial.println("A selected.");
    //vSendUDP(A,1);
    vSendTCP(_PLAYER_ID,A);
  }
}

void loop() {
  
  if(millis()-temps>=100){
    //TCPServer ();
    printStatus();
  //client.print("12");
  //client.stop();
    temps=millis();
  }
}


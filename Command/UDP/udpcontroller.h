#ifndef UDPCONTROLLER_H
#define UDPCONTROLLER_H

#include <QWidget>
#include <QtNetwork>

#define TX 45456

#define _INPUT_START 0
#define _INPUT_SELECT 4
#define _INPUT_UP 13
#define _INPUT_DOWN 12
#define _INPUT_LEFT 14
#define _INPUT_RIGHT 5
#define _INPUT_B 2
#define _INPUT_A 16

namespace Ui {
class udpController;
}

class udpController : public QWidget
{
    Q_OBJECT

public:
    explicit udpController(QWidget *parent = 0);
    ~udpController();

private slots:
    void vDatagramaUdpRxRebut();

private:
    QUdpSocket *udpSocketUdpRx;
    void vInterpretaTramaRebudaUdp(QByteArray datagrama,QHostAddress adrecaOrigen);

private:
    Ui::udpController *ui;
};

#endif // UDPCONTROLLER_H

#include "udpcontroller.h"
#include "ui_udpcontroller.h"

udpController::udpController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::udpController)
{
    ui->setupUi(this);
    udpSocketUdpRx = new QUdpSocket(this);
    udpSocketUdpRx->bind(TX);
    connect(udpSocketUdpRx, SIGNAL(readyRead()),this, SLOT(vDatagramaUdpRxRebut()));

}

udpController::~udpController()
{
    delete ui;
}

void udpController::vDatagramaUdpRxRebut(){
    while (udpSocketUdpRx->hasPendingDatagrams()) {
         QByteArray datagram;
         QHostAddress adrecaOrigen;
         quint16 portOrigen;
         datagram.resize(udpSocketUdpRx->pendingDatagramSize());
         udpSocketUdpRx->readDatagram(datagram.data(),datagram.size(),&adrecaOrigen,&portOrigen);
         vInterpretaTramaRebudaUdp(datagram,adrecaOrigen);
     }
}
void udpController::vInterpretaTramaRebudaUdp(QByteArray datagrama,QHostAddress adrecaOrigen){
    QString textRebut = datagrama.data();
    QStringList strList = textRebut.split(":");
    int numRebut = (strList.at(1)).toInt();
    qDebug() << "Recibido: "<< strList << " es decir: "<< strList.at(0) << " con valor " << numRebut << " de la dirección " << adrecaOrigen.toString();
    switch((strList.at(0)).toInt()){
        case _INPUT_START:
            qDebug() << "START selected!";
        break;
        case _INPUT_SELECT:
            qDebug() << "SELECT selected!";
        break;
        case _INPUT_UP:
            qDebug() << "UP selected!";
        break;
        case _INPUT_DOWN:
            qDebug() << "DOWN selected!";
        break;
        case _INPUT_LEFT:
            qDebug() << "LEFT selected!";
        break;
        case _INPUT_RIGHT:
            qDebug() << "RIGHT selected!";
        break;
        case _INPUT_B:
            qDebug() << "B selected!";
        break;
        case _INPUT_A:
            qDebug() << "A selected!";
        break;
    }
}

#-------------------------------------------------
#
# Project created by QtCreator 2018-05-20T17:53:03
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UDP
TEMPLATE = app


SOURCES += main.cpp\
        udpcontroller.cpp

HEADERS  += udpcontroller.h

FORMS    += udpcontroller.ui

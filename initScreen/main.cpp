#include "mainmenu.h"
#include <QApplication>
#include <QMovie>
#include <QGraphicsScene>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QDesktopWidget>
#include <QIcon>
#include <QGraphicsSceneMouseEvent>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainMenu *w = new MainMenu;
    w->setAttribute(Qt::WA_TranslucentBackground);
     //Imagen fondo bandera EEUU
    QGraphicsScene scene;
    QLabel *back_gif = new QLabel;
    QMovie *movie = new QMovie(":/wavingEEUUflag.gif");
    back_gif->setMovie(movie);
    w->setFixedSize(800,500);
    movie->start();
    scene.addWidget(back_gif);
    QGraphicsView view(&scene);
    view.move(QApplication::desktop()->screenGeometry().width()/2-400,QApplication::desktop()->screenGeometry().height()/2-250);
    view.setFixedSize(800,500);
    view.setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    view.setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    view.setLayout(w->layout());
    view.window()->setWindowTitle("PumpTrump"); //Titulo programa
    view.window()->setWindowIcon(QIcon(QString("../game/sprites/stuff/icon.ico"))); //Imagen Trump
    view.show();

    return a.exec();
}

#include "mainmenu.h"
#include "ui_mainmenu.h"
#include "createroom.h"
#include "joinroom.h"
#include "QDialog"


MainMenu::MainMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    //Botones para crear un server o cliente
    QObject::connect(ui->btnCreateRoom,SIGNAL( clicked() ),this,SLOT( createRoom() ) );
    QObject::connect(ui->btnJoinRoom,SIGNAL( clicked() ),this,SLOT( joinRoom() ) );

}
/**
 * Ejecuta el layout del server
 */
void MainMenu::createRoom(){
    CreateRoom aboutUi;
    aboutUi.setModal(true);
    aboutUi.exec();
}
/**
 * Ejecuta el layout del cliente
 */
void MainMenu::joinRoom(){
    JoinRoom aboutUi;
    aboutUi.setModal(true);
    aboutUi.exec();
}


MainMenu::~MainMenu()
{
    delete ui;
}

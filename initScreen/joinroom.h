#ifndef JOINROOM_H
#define JOINROOM_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class JoinRoom;
}

class JoinRoom : public QDialog
{
    Q_OBJECT

public:
    explicit JoinRoom(QWidget *parent = 0);
    ~JoinRoom();
public slots:
    void joinToRoom();
    void controller();
private:
    Ui::JoinRoom *ui;
};

#endif // JOINROOM_H

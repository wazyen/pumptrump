#include "joinroom.h"
#include "ui_joinroom.h"
#include <unistd.h>
#include <QMessageBox>
#include <QDebug>


JoinRoom::JoinRoom(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::JoinRoom)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    setFixedSize(size());

    //Eventos al clicar un boton del layout
    QObject::connect(ui->btnOk,SIGNAL(clicked()), this, SLOT( joinToRoom() ));
    QObject::connect(ui->btnOk,SIGNAL(clicked()), this, SLOT( accept() ));
    QObject::connect(ui->btnController, SIGNAL(clicked()), this, SLOT(controller()));
    QObject::connect(ui->btnKeyboard, SIGNAL(clicked()), this, SLOT(controller()));

}
/**
 * Funcion que permite al cliente conectarse por teclado al apretar el boton de keyboard
 */
void JoinRoom::controller()
{

    QObject* button = QObject::sender();
    //Si pulsamos en keyboard habilita los campos de conexion del teclado
    if (button == ui->btnController){
        ui->btnKeyboard->setChecked(0);
        ui->btnController->setChecked(1);
        ui->boxPlayer->setDisabled(1);
        ui->lePortServer->setDisabled(1);
        ui->leIpServer->setDisabled(1);
    }else{
        ui->btnController->setChecked(0);
        ui->btnKeyboard->setChecked(1);
        ui->boxPlayer->setEnabled(1);
        ui->lePortServer->setEnabled(1);
        ui->leIpServer->setEnabled(1);
    }
}
/**
 * Funcion que recoge los datos del form y ejecuta el cliente con los argumentos del formulario
 */
void JoinRoom::joinToRoom(){



    //Se crea un fork() para que el hijo ejecute el programa "game"
    int i = fork();
    if(i!=0){

    }else{
        QString player="0";
        char *cmd = (char *) "../game/PumpTrump";
        char *argv[6];
        argv[0] = (char *) "PumpTrump";

        char *data;

        //Si el keyboard esta chequeado se pasan argumentos de conexion por teclado, sino solo el puerto broadcast y player
        if(ui->btnKeyboard->isChecked()){

            player = QString::number(ui->boxPlayer->currentIndex()+1);

            //Puerto broadcast
            data = ui->lePortBroadcast->text().toLocal8Bit().data();
            argv[1] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[1],data);
            //Numero de player
            data = player.toLocal8Bit().data();
            argv[2] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[2],data);
            //Ip server
            data = ui->leIpServer->text().toLocal8Bit().data();
            argv[3] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[3],data);
            //Puerto server
            data = ui->lePortServer->text().toLocal8Bit().data();
            argv[4] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[4],data);
            argv[5] = NULL;


        }else{
            //Puerto broadcast
            data = ui->lePortBroadcast->text().toLocal8Bit().data();
            argv[1] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[1],data);
            //Numero de player
            data = player.toLocal8Bit().data();
            argv[2] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[2],data);

            argv[3] = NULL;
        }
        //Ejecutamos el programa game
        execvp(cmd, argv);

        qDebug() << "Error. Problems running the Game.";
        exit(1);
    }
    exit(0);
}


JoinRoom::~JoinRoom()
{
    delete ui;
}

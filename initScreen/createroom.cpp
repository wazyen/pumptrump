#include "createroom.h"
#include "ui_createroom.h"
#include <QNetworkInterface>
#include <unistd.h>
#include <QDir>
#include <QMessageBox>
#include <QDebug>


CreateRoom::CreateRoom(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::createRoom)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    setFixedSize(size());
    updateIP();

    //random
    srand ( time(NULL) );
    //generamos valor aleatorio para utilizar entre los puertos que estan registrados
    QString randPort = QString::number(rand() % 49151 + 1024);

    ui->lePortServer->setText(randPort);
    QObject::connect(ui->btnUpdateIPServer,SIGNAL( clicked() ),this,SLOT( updateIP() ) );
    QObject::connect(ui->Ok,SIGNAL( clicked()),this,SLOT( create() ) );
    QObject::connect(ui->btnPortPhysical,SIGNAL( clicked() ),this,SLOT( portPhysical() ) );

}
/**
 * Funcion que inserta por defecto el puerto broadcast para el mando fisico
 */
void CreateRoom::portPhysical(){
    ui->lePortServer->setText("45456");
}
/**
 * Metodo que inserta automaticamente la direccion ip/puerto server y ip broadcast
 */
void CreateRoom::updateIP(){
    ui->cbIPServer->clear();
    srand ( time(NULL) );

    foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces())
    {
        if (interface.flags().testFlag(QNetworkInterface::IsUp) && !interface.flags().testFlag(QNetworkInterface::IsLoopBack)){
            foreach (QNetworkAddressEntry entry, interface.addressEntries())
            {
                if ( interface.hardwareAddress() != "00:00:00:00:00:00" && entry.ip().toString().contains(".") && !interface.humanReadableName().contains("VM"))
                    //Ip del pc que hace de server
                    ui->cbIPServer->addItem(entry.ip().toString());
                if(!entry.broadcast().toString().isEmpty()){
                    //genera valor aleatorio para el puerto del server
                    ui->leIPBroadcast->setText(entry.broadcast().toString());
                    QString randPortBroad = QString::number((rand()+1) % 49151 + 1024);
                    ui->lePortBroadcast->setText(randPortBroad);
                }

            }
        }
    }
}

/**
 * Funcion que recoje los datos del form y ejecuta el programa Server con los argumentos del formulario
 */
void CreateRoom::create(){


    if(ui->lePortServer->text().isEmpty() || ui->leIPBroadcast->text().isEmpty() || ui->lePortBroadcast->text().isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("Please, make sure you fill in all fields.");
        msgBox.setWindowTitle("Error");
        msgBox.exec();
    }else{


        int i = fork();
        if(i!=0){

        }else{


            char *cmd = (char *) "../Server/Server";
            char *argv[8];
            char *data;
            argv[0] = (char *) "Server";

            //Argumentos que pasamos al server
            //Puerto server
            data = ui->lePortServer->text().toLocal8Bit().data();
            argv[1] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[1],data);
            //Ip broadcast
            data = ui->leIPBroadcast->text().toLocal8Bit().data();
            argv[2] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[2],data);
            //Puerto broadcast
            data = ui->lePortBroadcast->text().toLocal8Bit().data();
            argv[3] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[3],data);
            //Numero de players
            data = ui->nPlayers->currentText().toLocal8Bit().data();
            argv[4] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[4],data);
            //Numero de vidas
            data = ui->nLives->currentText().toLocal8Bit().data();
            argv[5] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[5],data);
            //Ip Server
            data = ui->cbIPServer->currentText().toLocal8Bit().data();
            argv[6] = (char *) malloc(sizeof(char)*strlen(data));
            strcpy(argv[6],data);

            argv[7] = NULL;

            //Ejecuta el programa server
            execvp(cmd, argv);

            qDebug() << "Error. Problems running the Server.";

            exit(1);
        }
    }
}

CreateRoom::~CreateRoom()
{
    delete ui;
}

#-------------------------------------------------
#
# Project created by QtCreator 2018-05-25T19:01:24
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mainScreen
TEMPLATE = app


SOURCES += main.cpp\
        mainmenu.cpp \
    createroom.cpp \
    joinroom.cpp

HEADERS  += mainmenu.h \
    createroom.h \
    joinroom.h

FORMS    += mainmenu.ui \
    createroom.ui \
    joinroom.ui

DISTFILES +=

RESOURCES += \
    resources.qrc

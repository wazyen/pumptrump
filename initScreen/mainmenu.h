#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QPixmap>


namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = 0);
    ~MainMenu();

private:
    Ui::MainMenu *ui;
public slots:
    void createRoom();
    void joinRoom();
};

#endif // MAINMENU_H

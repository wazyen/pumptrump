#ifndef CREATEROOM_H
#define CREATEROOM_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class createRoom;
}

class CreateRoom : public QDialog
{
    Q_OBJECT

public:
    explicit CreateRoom(QWidget *parent = 0);
    ~CreateRoom();
public slots:
    void updateIP();
    void create();
    void portPhysical();

private:
    Ui::createRoom *ui;
};

#endif // CREATEROOM_H

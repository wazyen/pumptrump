#ifndef SERVER_H
#define SERVER_H

#include <QWidget>
#include "QDebug"
#include <QMessageBox>
#include <QWidget>
#include <QPixmap>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netinet/in.h>
#include <resolv.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <QTimer>
#include <QDateTime>

#include "QDialog"

//Maximo clientes
#define MAX_CHILDS 10
#define BUFFERSIZE 256
namespace Ui {
class Server;
}

class Server : public QWidget
{
    Q_OBJECT

public:
    explicit Server(int argc, char **argv, QWidget *parent = 0);
    ~Server();
    int server(int puerto, char* broadcastIP, unsigned broadcastPort, unsigned players, unsigned lives);
    void error(char *errorMessage);
    void waitConnect(int loop);
    int stopClient(int socket);
    int sendMessage(char *broadcastIP, short broadcastPort, char sendString [BUFFERSIZE]);
    int readCliente(int socket, char *broadcastIP, unsigned broadcastPort);
    //static void * serverThread( void *param );

private:
    Ui::Server *ui;
};

#endif // SERVER_H

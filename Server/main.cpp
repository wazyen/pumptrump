#include "server.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Server* w = new Server(argc,argv);
    w->show();

    return a.exec();
}

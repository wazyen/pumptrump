#include "server.h"
#include "ui_server.h"
#include <deque>
#include <memory>


/*Struct utilizado para pasar datos en el createThread()*/
typedef struct sConfig{
    Server *p;
    int port;
    char *broadcastIP;
    unsigned broadcastPort;
    unsigned players;
    unsigned lives;
} Config;

/**
 * Funcion que utiliza un thread par ejecutar la funcion server
 * @param param : struct sConfig
 * @return
 */
static void * serverThread( void *param )
{
    sConfig *config = (sConfig *) param;
    config->p->server(config->port,config->broadcastIP,config->broadcastPort, config->players, config->lives);
    pthread_exit(NULL);
}



Server::Server(int argc, char **argv, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Server)
{
    ui->setupUi(this);
    setFixedSize(size());
    //Especifica los argumentos
    if (argc < 6) {
        printf("%s <Port server> <IP broadcast> <Port broadcast> <Number of players> <Number of lives><Ip Server>\n", argv[0]);
        exit(1);
    }

    int portServer = atoi(argv[1]); //Primer arg: Puerto server - app
    char * broadcastIP = argv[2]; //Segundo arg:  broadcast IP
    int broadcastPort = atoi(argv[3]); //Tercer arg:  broadcast puerto
    unsigned players = atoi(argv[4]); //Cuarto arg:  numero players
    unsigned lives = atoi(argv[5]); //Quinto arg:  lives
    char * serverIP = argv[6]; //Sexto arg:  ip server

    //Introducimos informacion de las conexiones en el layout
    ui->lePortServer->setText(QString::fromStdString(argv[1]));
    ui->leIpBroadcast->setText(QString::fromStdString(broadcastIP));
    ui->lePortBroadcast->setText(QString::fromStdString(argv[3]));
    ui->leIpServer->setText(QString::fromStdString(serverIP));
    //Barra de progreso al conectarse los clientes
    ui->progressBarClients->setOrientation(Qt::Horizontal);
    ui->progressBarClients->setRange(0, 100); //Porcentaje del progressBar
    ui->progressBarClients->setValue(0); // Inicio 0

    sConfig *config = new sConfig;
    config->p=this;
    config->port=portServer;
    config->broadcastIP=broadcastIP;
    config->broadcastPort=broadcastPort;
    config->players=players;
    config->lives=lives;
    pthread_t tid;
    //Creamos un thread para que ejecute la funcion server
    pthread_create( &tid, NULL, serverThread, (void *)config);

}


Server::~Server()
{
    delete ui;
}

/**
 * Visualiza los errores que puede ocasionar una conexion
 * @param errorMessage
 */
void Server::error(char *errorMessage) {
    perror(errorMessage);
    exit(1);
}

/**
 * Visualiza la espera de una nueva conexion
 * @param loop
 */
void Server::waitConnect(int loop) {
    if (loop == 0) {
        printf("[SERVER] Waiting for connection  ");

    }

    switch (loop % 4) {
    case 0: //printf("|");
        break;
    case 1: //printf("\\");
        break;
    case 2: //printf("-");
        break;
    case 3: //printf("/");
        break;
    default:
        break;
    }


    fflush(stdout); //Actualizamos la pantalla
}

/**
 * Limite de maximos clientes conectados
 * @param socket
 * @return
 */
int Server::stopClient(int socket) {
    char buffer[BUFFERSIZE];
    int bytecount;

    memset(buffer, 0, BUFFERSIZE);

    sprintf(buffer, "Many connected clients.\n");

    if ((bytecount = send(socket, buffer, strlen(buffer), 0)) == -1)
        error((char *)"I can't send information");

    ::close(socket);

    return 0;
}


/**
  Metodo que recibe por parametro la informacion que recibe el server y la envia al cliente
 * @brief Server::sendMessage
 * @param broadcastIP
 * @param broadcastPort
 * @param sendString: datos
 * @return
 */
int Server::sendMessage(char *broadcastIP, short broadcastPort, char sendString [BUFFERSIZE]) {

    int sock;
    struct sockaddr_in broadcastAddr; //Direccion broadcast
    int broadcastPermission; //Permiso socket



    // Crea socket para el envio de datos
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error((char *)"Error in the creation of socket broadcast");


    broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
                   sizeof (broadcastPermission)) < 0)
        error((char *)"Error setsockopt()");

    //Contructor direccion local
    memset(&broadcastAddr, 0, sizeof (broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP); //Direccion IP Broadcast
    broadcastAddr.sin_port = htons(broadcastPort); //Puerto Broadcast

    unsigned int sendStringLen = strlen(sendString);

    //Envio datos a los clientes via broadcast
    if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *) &broadcastAddr, sizeof (broadcastAddr)) != sendStringLen) {
        error((char *)"Sending different bytes than expected.");
    }

    return 0;
}

/**
 * Lee los datos enviandos del mando fisico, app y teclado
 * @param socket
 * @param addr
 * @return
 */
int Server::readCliente(int socket, char *broadcastIP, unsigned broadcastPort) {

    char buffer[BUFFERSIZE];
    int bytecount;
    int fin = 0;
    int code = 0;

    //El cliente siempre esta a la escucha hasta su desconexion
    while (!fin) {

        memset(buffer, 0, BUFFERSIZE);
        if ((bytecount = recv(socket, buffer, BUFFERSIZE, 0)) == -1) {
            error((char *)"I can't receive information");
        }


        //Datos recibidos. Numero de player y tecla apretada (Solo visualizacion)
        switch (buffer[1]) {
        case '1':
            printf("Server: Player: %c - Key: UP %c\n", buffer[0],buffer[2]);
            break;
        case '2':
            printf("Server: Player: %c - Key: DOWN %c\n", buffer[0],buffer[2]);
            break;
        case '3':
            printf("Server: Player: %c - Key: RIGHT %c\n", buffer[0],buffer[2]);
            break;
        case '4':
            printf("Server: Player: %c - Key: LEFT %c\n", buffer[0],buffer[2]);
            break;
        case '5':
            printf("Server: Player: %c - Key: SELECT\n", buffer[0]);
            break;
        case '6':
            printf("Server: Player: %c - Key: START\n", buffer[0]);
            break;
        case '7':
            printf("Server: Player: %c - Key: A\n", buffer[0]);
            break;
        case '8':
            printf("Server: Player: %c - Key: B\n", buffer[0]);
            break;
        default:
            printf("Server: Player: %c Desconectado.\n\n", buffer[0]);
            //Se desconecta cliente
            //fin = 1;

            break;
        }

        if (!fin) {
            //Envio de datos a todos los clientes via Broadcast
            sendMessage(broadcastIP, broadcastPort, buffer);
        }


        if ((bytecount = send(socket, buffer, strlen(buffer), 0)) == -1) {
            error((char *)"I can't receive information");
        }
    }

    ::close(socket);
    return code;
}

/*
 * El proceso padre escucha en el puerto esperado para una nueva conexion y
 * cuando se realiza el proceso se bifurca a un proceso hijo para la lectura del
 * socket que es enviando desde la app.
 */

int Server::server(int puerto, char* broadcastIP, unsigned broadcastPort, unsigned players, unsigned lives){


    QString time_text= "";
    unsigned clients = 0;
    int porcentaje = 0;
    int socket_host;
    struct sockaddr_in client_addr;
    struct sockaddr_in my_addr;
    struct timeval tv; // Para el timeout del accept
    socklen_t size_addr = 0;
    int socket_client;
    fd_set rfds; //Conjunto de descriptores a vigilar
    int childcount = 0;
    int exitcode;

    int childpid;
    int pidstatus;

    int activated = 1;
    int loop = 0;
    socket_host = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_host == -1)
        error((char *)"The socket can't be initialized");

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(puerto);
    my_addr.sin_addr.s_addr = INADDR_ANY;

    //Error a la hora de conectar
    if (bind(socket_host, (struct sockaddr*) &my_addr, sizeof (my_addr)) == -1)
        error((char *)"The port is in use");

    if (listen(socket_host, 10) == -1)
        error((char *)"Error in the port");

    size_addr = sizeof (struct sockaddr_in);

    while (activated) {

        waitConnect(loop);
        //se carga el valor de rfds
        FD_ZERO(&rfds);
        FD_SET(socket_host, &rfds);


        tv.tv_sec = 0;
        tv.tv_usec = 500000; //Tiempo de espera

        //El server espera a la conexion de clientes
        if (select(socket_host + 1, &rfds, NULL, NULL, &tv)) {
            if ((socket_client = accept(socket_host, (struct sockaddr*) &client_addr, &size_addr)) != -1) {

                loop = -1; // Para reiniciar el mensaje de Esperando conexión
                printf("\nConnected client: %s Port: %d\n", inet_ntoa(client_addr.sin_addr), client_addr.sin_port);

                //Visualizamos el tiempo de conexion del cliente
                QTime time = QTime::currentTime();

                clients++;
                //Barra de progreso de clientes conectados
                if(players == clients) porcentaje  = 100;
                else  porcentaje = (100 / players) * clients;
                ui->progressBarClients->setValue(porcentaje); // valor del progressbar
                //Informacion del cliente
                char *buf= (char *) malloc(sizeof(char)*BUFFERSIZE);
                sprintf(buf,"Connected client %d:  %s  Port: %d  at %s",clients, inet_ntoa(client_addr.sin_addr), client_addr.sin_port,time.toString("hh:mm:ss").toLatin1().data());
                ui->textInfo->append(buf);

                switch (childpid = fork()) {
                case -1: //Error
                    error((char *)"Error creating the child process");
                    break;
                case 0: // proceso hijo
                {
                    //Enviamos socket de inicio de juego al cliente con las vidas y el num de jugadores
                    char *buffer= (char *) malloc(sizeof(char)*4);
                    sprintf(buffer,"i%d%d",players,lives);
                    printf("buffer: %s\n", buffer);
                    sendMessage(broadcastIP,broadcastPort, buffer);

                    //Maximo 10 clientes conectados, se puede cambiar la constante
                    if (childcount < MAX_CHILDS) {
                        //readCliente estara a la espera de sockets
                        exitcode = readCliente(socket_client, broadcastIP, broadcastPort);
                    } else {
                        //Informa al usuario que hay mechos clientes conectados
                        exitcode = stopClient(socket_client);
                    }
                    exit(exitcode); //Código de salida
                    break;
                }
                default: //proceso padre
                    childcount++; //Un nuevo hijo
                    ::close(socket_client);
                    break;
                }
            } else {
                fprintf(stderr, "ERROR WHEN ACCEPTING THE CONNECTION.\n");
            }
        }

        //Miramos si se ha cerrado algún hijo últimamente
        childpid = waitpid(0, &pidstatus, WNOHANG);
        if (childpid > 0) {
            childcount--; //Muere un hijo

            if (WIFEXITED(pidstatus)) {

                if (WEXITSTATUS(pidstatus) == 99) {
                    printf("\nThe program is closed.\n");
                    activated = 0;
                }
            }
        }
        loop++;
    }

    ::close(socket_host);

    return 0;
}


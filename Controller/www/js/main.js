/**
 * Pantalla de inicio de aplicacion. Muestra la imagen de la aplicacion mientras se carga la app
 * @param {type} param
 */
$(document).ready(function () {

    document.addEventListener("deviceready", onDeviceReady, false);

    //El evento se dispara cuando Cordova está completamente cargado.
    function onDeviceReady() {
        localStorage.clear();

        var timer;
        location.href = 'connection.html';

    }
});

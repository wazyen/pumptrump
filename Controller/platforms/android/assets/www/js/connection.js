/**
 * Pantalla de conexion al servidor. Datos del server y numero de jugador
 * @param {type} param
 */
$(document).ready(function () {


    //Evento al pulsar boton atras <--
    document.addEventListener("backbutton", onBackKeyDown, false);

    //Variables de sesion
    var sessionIp = localStorage.getItem('ip');
    var sessionPort = localStorage.getItem('port');
    var sessionPlayer = localStorage.getItem('player');
    document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    function onDeviceReady() {

        if (sessionIp) {

            location.href = 'principal.html';

        }
    }

    //Evento al pulsar el boton de registro. Datos de la conexion
    $("#btn-login").click(function () {

        var id_ip = $("#id_ip").val(); //Ip
        var puerto = $("#inputPort").val(); //Puerto
        var jugador = $("#inputPlayer").val(); //Numero de jugador
        

        //Lo guardamos en variables de session
        sessionIp = id_ip;
        localStorage.setItem('ip', sessionIp);
        sessionPort = puerto;
        localStorage.setItem('port', sessionPort);
        sessionPlayer = jugador;
        localStorage.setItem('player', sessionPlayer);


    });

    //Funcion boton atras app
    function onBackKeyDown() {
        $('#myModal').modal('show');
    }
    //Boton confirma SI cerrar session
    $("#closeApp").on('click', function () {

        if (navigator.app) {
            navigator.app.exitApp();
        } else if (navigator.device) {
            navigator.device.exitApp();
        } else {

            window.close();
        }

    });
    //Boton confirma No cerrar session
    $(".btn-danger").on('click', function () {
        $('#myModal').modal('hide');
    });

});

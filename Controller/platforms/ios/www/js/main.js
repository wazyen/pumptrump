/**
 * Pantalla de inicio de aplicacion. Muestra la imagen de la aplicacion mientras se carga la app
 * @param {type} param
 */
$(document).ready(function () {


    document.addEventListener("deviceready", onDeviceReady, false);

    //El evento se dispara cuando Cordova está completamente cargado.
    function onDeviceReady() {
        localStorage.clear();

        var timer;
        location.href = 'connection.html';

// Saber si hay conexion internet
// 
//        var connectedRef = firebase.database().ref(".info/connected");
//        connectedRef.on("value", function (snap) {
//
//            if (snap.val() === true) {
//                clearTimeout(timer);
//
//                  location.href = 'connection.html';
//            } else {
//
//                timer = setTimeout(function () {
//
//
//                    navigator.notification.confirm(
//                            'No hay conexion a internet', // message
//                            onConfirm, // callback to invoke with index of button pressed
//                            'Aviso', // title
//                            ['Reintentar', 'Salir']     // buttonLabels
//                            );
//
//                }, 5000);
//
//            }
//        });

        function onConfirm(buttonIndex) {

            //Cierra app
            if (buttonIndex == 2) {

                if (navigator.app) {
                    navigator.app.exitApp();
                } else if (navigator.device) {
                    navigator.device.exitApp();
                } else {

                    window.close();
                }
            } else {
                //Reintentar conexion
                onDeviceReady();
            }
        }

    }

});

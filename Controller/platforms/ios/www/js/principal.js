/**
 * Pantalla principal del mando. Envio de datagramas al servidor. 
 * Permite la conexion y desconexion del mando
 * @param {type} param
 */
$(document).ready(function () {

    //Evento al pulsar boton atras <--
    document.addEventListener("backbutton", onBackKeyDown, false);

    //Recogemos datos de las variables de session
    var ip = localStorage.getItem('ip');
    var port = localStorage.getItem('port');
    var player = localStorage.getItem('player');
 

    //Cambia el color del nombre del juego depende del jugador seleccionado
    if (player == "2")
        $("#idNavbar").css({"color": "#1111A9"});
    else if (player == "3")
        $("#idNavbar").css({"color": "#AA0000"});
    else if (player == "4")
        $("#idNavbar").css({"color": "#971D97"});
    


    //Evento de abrir conexiones
    $("#idConnection").click(function () {
        $("#inputPlayer option[value=" + player + "]").attr("selected", true);
    });
    
    //Evento que escucha el boton de conectar
    $("#btn-anadir").click(function () {

        var ipServer = document.forms["myForm"]["txtIp"].value;
        var portServer = parseInt(document.forms["myForm"]["txtPort"].value);
        var players = document.forms["myForm"]["txtPlayer"].value;

        player = players;
        ip = ipServer;
        port = portServer;

    });

    document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    function onDeviceReady() {

        //Creamos objecto 
        var socket = new Socket();

        socket.onData = function (data) {
            // alert("ondata");
            // invoked after new batch of data is received
        };
        socket.onError = function (errorMessage) {
            navigator.notification.confirm(
                    'Error en la conexion',
                    onConfirm, // callback to invoke with index of button pressed
                    'Aviso', // title
                    ['Reintentar', 'Salir']     // buttonLabels
                    );
            // Error durante la conexion
        };
        socket.onClose = function (hasError) {
            //Desconectado cliente
            window.plugins.toast.showShortCenter('Desconectado', function (a) {
                console.log('toast success: ' + a)
            }, function (b) {
                alert('toast error: ' + b);
            });
        };
        socket.open(
                ip,
                port,
                function () {

                  


                    //Conectado con el servidor
                    $('#conect').fadeIn(1000);
                    // alert(ip + " - " + port + " - " + player);
                    window.plugins.toast.showShortCenter('Conectado', function (a) {
                        console.log('toast success: ' + a)
                    }, function (b) {
                        alert('toast error: ' + b);
                    });



                    /*
                     * var message: "<player><key><0 o 1>"
                     * Up: 1 Recargar balas
                     * DOWN: Proteger
                     * RiGHT: 3
                     * LEFT: 4
                     * SELECT: 5
                     * START: 6
                     * A: 7 Saltar
                     * B: 8 Disparar
                     * 
                     */
                    $("#conect").click(function () {

                        $('#conect').fadeOut(1000);

                        message = player + "010";
                        sendMessage(message);
                    });
                    var message = "";



                    //boton recarga balas. Boton largo
                    document.getElementById("btnUp").addEventListener('touchstart', function () {
                        message = player + "110";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btnUp").addEventListener('touchend', function () {
                        message = player + "100";
                        sendMessage(message);

                    }, false);
                  
                   
                       //boton down: proteger
                    document.getElementById("btnDown").addEventListener('touchstart', function () {
                        message = player + "210";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btnDown").addEventListener('touchend', function () {
                        message = player + "200";
                        sendMessage(message);

                    }, false);
                    //boton right. Boton largo
                    document.getElementById("btnRight").addEventListener('touchstart', function () {
                        message = player + "310";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btnRight").addEventListener('touchend', function () {
                        message = player + "300";
                        sendMessage(message);

                    }, false);
                    //boton left. Boton largo
                    document.getElementById("btnLeft").addEventListener('touchstart', function () {
                        message = player + "410";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btnLeft").addEventListener('touchend', function () {
                        message = player + "400";
                        sendMessage(message);

                    }, false);
                    //boton select
                    $("#btn1").click(function () {
                        message = player + "510";
                        sendMessage(message);
                    });
                    //boton start
                    $("#btn2").click(function () {
                        message = player + "610";
                        sendMessage(message);
                    });
                    //boton A: saltar
                    
                    document.getElementById("btn3").addEventListener('touchstart', function () {
                        message = player + "710";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btn3").addEventListener('touchend', function () {
                        message = player + "700";
                        sendMessage(message);

                    }, false);
                    
                    //boton B: disparar
                     document.getElementById("btn4").addEventListener('touchstart', function () {
                        message = player + "810";
                        sendMessage(message);

                    }, false);
                    document.getElementById("btn4").addEventListener('touchend', function () {
                        message = player + "800";
                        sendMessage(message);

                    }, false);
                    
                    
                    //Envia socket al programa principal
                    function sendMessage(message) {
                        var data = new Uint8Array(message.length);
                        for (var i = 0; i < data.length - 1; i++) {
                            data[i] = message.charCodeAt(i);
                        }
                        socket.write(data);
                    }
                },
                function (errorMessage) {

                });
        socket.shutdownWrite();

    }


    function onConfirm(buttonIndex) {

        //Cierra app
        if (buttonIndex == 2) {

            if (navigator.app) {
                navigator.app.exitApp();
            } else if (navigator.device) {
                navigator.device.exitApp();
            } else {

                window.close();
            }
        } else {
            //Reintentar conexion
            onDeviceReady();
        }

    }

    //Evento que escucha al pulsar el boton exit
    $("#exit").on('click', function () {
        $('#myModal').modal('show');
    });
    //Funcion boton atras app
    function onBackKeyDown() {
        $('#myModal').modal('show');
    }

    //Boton confirma SI cerrar session
    $("#closeApp").on('click', function () {

        if (navigator.app) {
            navigator.app.exitApp();
        } else if (navigator.device) {
            navigator.device.exitApp();
        } else {

            window.close();
        }

    });
    //Boton confirma No cerrar session
    $(".btn-danger").on('click', function () {
        $('#myModal').modal('hide');
    });
});


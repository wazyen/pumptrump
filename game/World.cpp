#include "World.h"
#include "ContactListener.h"

#define MAXRECVSTRING 255

static void * readServer( void *param )
{
    World *world = ((readServerParam *) param)->world;
    int sock;
    struct sockaddr_in broadcastAddr; // Broadcast Address
    unsigned short broadcastPort; //Primer arg: Port broadcast
    char recvString[MAXRECVSTRING + 1]; //Buffer
    int recvStringLen; // Length of received string
    int fin = 0;

    broadcastPort = atoi(((readServerParam *) param)->readPort); // First arg: broadcast port

    printf("broadcast port: -%d-\n", broadcastPort);
    // We create a socket using UDP
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        qDebug() << "[CLIENT] Error while trying to create the socket";
        exit(0);
    }

    //Construct bind structure
    memset(&broadcastAddr, 0, sizeof (broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY); //Cualquier interface entrante
    broadcastAddr.sin_port = htons(broadcastPort); //Broadcast port

    //Bind broadcast port
    if (bind(sock, (struct sockaddr *) &broadcastAddr, sizeof (broadcastAddr)) < 0)
    {
        qDebug() << "[CLIENT] Broadcast port bind failed";
        fprintf(stderr,"%s",strerror(errno));
        exit(0);
    }
//int datosRecibidos=0;

    while (!fin)
    {
        //Recibe datos
        if ((recvStringLen = recvfrom(sock, recvString, MAXRECVSTRING, 0, NULL, 0)) < 0) {
            qDebug() << "Error datos";

        }
        //datosRecibidos++;
        recvString[recvStringLen] = '\0';

        // If we are configuring the game
        if(recvString[0]=='i')
        {
            // -48 comes from conversion from ASCII char to int
            world->players(recvString[1]-48);
            world->health(recvString[2]-48);
            pthread_cond_signal(world->getReady());
        }

        bool isPressed = true;
        if(strlen(recvString)>=3)
            if(recvString[2]=='0')
                isPressed = false;

        //Datos recibidos. Numero de player y tecla apretada (Solo visualizacion)
        switch (recvString[1]) {
        case '1':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Up, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Up, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '2':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Down, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Down, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '3':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Right, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Right, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '4':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Left, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Left, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '5':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Space, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Space, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '6':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Escape, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Escape, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '7':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_X, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_X, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        case '8':
        {
            if(isPressed)
                emit world->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_Z, Qt::NoModifier, QString(recvString[0])));
            else
                emit world->keyReleaseEvent(new QKeyEvent(QEvent::KeyRelease, Qt::Key_Z, Qt::NoModifier, QString(recvString[0])));
            break;
        }
        default:
            qDebug() << "Pressed key not recognized!";
            break;
        }
		//qDebug() << datosRecibidos;
    }
    close(sock);
    pthread_exit(0);
}

static void * tellServer( void *param )
{
    Monitor *monitor = ((tellServerParam *) param)->monitor;
    char *serverIP = ((tellServerParam *) param)->argv[3];
    char *writePort = ((tellServerParam *) param)->argv[4];

    qDebug() << "Server IP: " << serverIP << "Puerto Server: " << writePort;

    struct sockaddr_in client;
    struct hostent *server = gethostbyname(serverIP); //Asignar Ip server

    if (!server)
    {
        qDebug() << "Error server";
        exit(1);
    }

    // Crea socket para el envio de datos
    int connection = socket(AF_INET, SOCK_STREAM, 0);
    //bzero() es como memset() pero inicializa las variables
    bzero((char *) &client, sizeof ((char *) &client));
    client.sin_family = AF_INET; //asignacion del protocolo
    client.sin_port = htons((atoi(writePort))); //Asignar puerto
    bcopy((char *) server->h_addr, (char *) &client.sin_addr.s_addr, sizeof (server->h_length));

    // Connection with server
    if (connect(connection, (struct sockaddr *) &client, sizeof (client)) < 0)
    {
        qDebug() << "Error connection server";
        exit(1);
    }

    while(true)
    {
        pthread_mutex_lock(&monitor->lock);
        while(monitor->pressedKeys.isEmpty())
            pthread_cond_wait(&monitor->cond,&monitor->lock);
        pthread_mutex_unlock(&monitor->lock);
        char *out = monitor->pressedKeys.dequeue();
        qDebug() << "Mensaje: " << out;
        send(connection, out, 3, 0); //envio de sockets
    }

    pthread_exit(0);
}

static void * recharge( void *param )
{
    struct Player *player = (struct Player *) param;
    player->isCharging = true;
    player->alreadyCharging = true;
    while(player->ammo < 3 && player->isCharging)
    {
        sleep(1);
        if(player->isCharging)
        {
            player->ammo++;
            player->ammoLabel->setText(QString::number(player->ammo));
        }
    }
    player->alreadyCharging = false;
    pthread_exit(0);
}

World::World(char **argv) : _timerId(0), _keyboardPlayerID(argv[2][0]-48), _isReady(false), _getReady(PTHREAD_COND_INITIALIZER), _getReadyLock(PTHREAD_MUTEX_INITIALIZER)
{
    pthread_t tid[2];
    // We create a thread that receives information from the server
    readServerParam *readParam = new readServerParam;
    readParam->world = this;
    readParam->readPort = argv[1];
    pthread_create( &tid[0], NULL, readServer, (void *) readParam);

    //We create a thread that sends information to the server
    if(_keyboardPlayerID){
        tellServerParam *tellParam = new tellServerParam;
        tellParam->monitor = &_monitor;
        tellParam->argv = argv;
        pthread_create( &tid[1], NULL, tellServer, (void *) tellParam);
    }
    pthread_mutex_lock(&_getReadyLock);
    pthread_cond_wait(&_getReady, &_getReadyLock);
    pthread_mutex_unlock(&_getReadyLock);

    // We configure the world
    _transform.scale(10.0f, 10.0f);
    b2Vec2 gravity(0.0f, 140.0f);
    bool doSleep(true);
    _world = new b2World(gravity,doSleep);

    int width = QApplication::desktop()->screenGeometry().size().width();
    int height = QApplication::desktop()->screenGeometry().size().height();

    QVBoxLayout *mainLayout = new QVBoxLayout();
    setLayout(mainLayout);
    QHBoxLayout **hboxRow = (QHBoxLayout**) malloc(((_nPlayers+1)/2)*sizeof(QHBoxLayout*));
    _hboxPlayer = (QHBoxLayout**) malloc (_nPlayers*sizeof(QHBoxLayout*));
    QLabel **connection = (QLabel**) malloc (_nPlayers*sizeof(QLabel*));
    QLabel **name = (QLabel**) malloc (_nPlayers*sizeof(QLabel*));
    QHBoxLayout **hearts = (QHBoxLayout**) malloc (_nPlayers*sizeof(QHBoxLayout*));
    QLabel **bullets = (QLabel**) malloc (_nPlayers*sizeof(QLabel*));
    QLabel **ammo = (QLabel**) malloc (_nPlayers*sizeof(QLabel*));

    QFont font;
    font.setPointSize(20);
    QPixmap heartImage(":/sprites/stuff/heart.png");    //width/48.0f,height/31.1f

    unsigned i;
    for(i=0;i<_nPlayers;i++)
    {
        float32 xPos = width/132.0f;
        QString playerColor("QLabel { color : black; }");
        QString playerLabel("Player Black:");
        switch(i)
        {
            case 1: xPos = width/10.8f; playerColor = QString("QLabel { color : blue; }"); playerLabel = QString("Player Blue:"); break;
            case 2: xPos = width/32.0f; playerColor = QString("QLabel { color : red; }"); playerLabel = QString("Player Red:"); break;
            case 3: xPos = width/14.6f; playerColor = QString("QLabel { color : purple; }"); playerLabel = QString("Player Magenta:"); break;
            default: ;
        }
        _players.append(createPlayer(b2Vec2(xPos,height/10.0f-height/130.0f),i%2==0,i+1));
        _hboxPlayer[i] = new QHBoxLayout;
        _hboxPlayer[i]->addSpacerItem(new QSpacerItem(width/120.0f,0.0f));
        connection[i] = new QLabel();
        connection[i]->setAlignment(Qt::AlignCenter);
        connection[i]->setPixmap(QPixmap(":/sprites/stuff/disconnected.png").scaled(1.0f,height/31.1f, Qt::KeepAspectRatioByExpanding));
        _hboxPlayer[i]->addWidget(connection[i]);
        _hboxPlayer[i]->setAlignment(connection[i],Qt::AlignVCenter);
        _players.value(i)->connection = connection[i];
        _hboxPlayer[i]->addSpacerItem(new QSpacerItem(width/90.0f,0.0f));
        name[i] = new QLabel(playerLabel);
        name[i]->setStyleSheet(playerColor);
        name[i]->setFont(font);
        _hboxPlayer[i]->addWidget(name[i]);
        _hboxPlayer[i]->setAlignment(name[i],Qt::AlignVCenter);
        _hboxPlayer[i]->addSpacerItem(new QSpacerItem(width/90.0f,0.0f));
        hearts[i] = new QHBoxLayout;
        _hboxPlayer[i]->addLayout(hearts[i]);
        _hboxPlayer[i]->setAlignment(hearts[i],Qt::AlignVCenter);
        _hboxPlayer[i]->addSpacerItem(new QSpacerItem(width/90.0f,0.0f));
        bullets[i] = new QLabel();
        bullets[i]->setAlignment(Qt::AlignCenter);
        bullets[i]->setPixmap(QPixmap(":/sprites/stuff/bullet.png").scaled(1.0f,height/31.1f, Qt::KeepAspectRatioByExpanding));
        _hboxPlayer[i]->addWidget(bullets[i]);
        _hboxPlayer[i]->setAlignment(bullets[i],Qt::AlignVCenter);
        _players.value(i)->bullets = bullets[i];
        ammo[i] = new QLabel("3");
        ammo[i]->setFont(font);
        _hboxPlayer[i]->addWidget(ammo[i]);
        _hboxPlayer[i]->setAlignment(ammo[i],Qt::AlignVCenter);
        _players.value(i)->ammoLabel = ammo[i];
        if(i%2==0)
        {
            hboxRow[i/2] = new QHBoxLayout;
            mainLayout->addLayout(hboxRow[i/2]);
            hboxRow[i/2]->addLayout(_hboxPlayer[i]);
            hboxRow[i/2]->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
        }
        else
        {
            hboxRow[i/2]->addLayout(_hboxPlayer[i]);
        }

        unsigned j;
        for (j=0;j<_health;j++)
        {
            QLabel *heart = new QLabel();
            heart->setAlignment(Qt::AlignCenter);
            heart->setPixmap(heartImage.scaled(1.0f,height/31.1f, Qt::KeepAspectRatioByExpanding));
            hearts[i]->addWidget(heart);
            _players.value(i)->hearts.append(heart);
        }
    }
    mainLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    
    // We draw environment obstacles
    _objects.append(createWall(WallObject, width/64.0f, height/12.65f, width/126.0f, height/43.2f, 0.0f));//Make America sign
    _objects.append(createWall(Floor, width/64.0f, height/12.65f, width/126.0f, 0.0f, 0.0f));
    _objects.append(createWall(WallObject, width/25.65f, height/12.65f, width/44.9f, height/43.2f, 0.0f));//White house sign
    _objects.append(createWall(Floor, width/25.65f, height/12.65f, width/44.9f, 0.0f, 0.0f));
    _objects.append(createWall(WallObject, width/13.03f, height/12.65f, width/126.5f, height/43.2f, 0.0f));//Great again sign
    _objects.append(createWall(Floor, width/13.03f, height/12.65f, width/126.5f, 0.0f, 0.0f));

    _objects.append(createWall(Floor, width/37.2f, height/16.92f, width/52.1f, 0.0f, 0.0f));//Platform 1 (bot left)
    _objects.append(createWall(Floor, width/18.58f, height/31.0f, width/52.1f, 0.0f, 0.0f));//Platform 2 (top right)

    _objects.append(createWall(WallObject, 0.0f, -height/10.0f, 0.0f, height/5.0f, 0.0f));//Left Wall
    _objects.append(createWall(WallObject, width/10.0f, -height/10.0f, 0.0f, height/5.0f, 0.0f));//Right Wall
    _objects.append(createWall(Floor, 0.0f, height/10.06f, width/10.0f, 0.0f, 0.0f));//Base

    _contactListenerInstance = new ContactListener();
    _world->SetContactListener(_contactListenerInstance);
}

b2World* World::GetWorld() { return _world; }

pthread_cond_t* World::getReady() { return &_getReady; }

void World::players(int nPlayers) { _nPlayers = nPlayers; }

void World::health(int health) { _health = health; }

Object* World::createWall(int type, float32 x, float32 y, float32 w, float32 h, float32 angle)
{
    Object *wall = new Object;
    // body
    b2BodyDef bd;
    bd.type = b2_staticBody;
    bd.position = b2Vec2(x+w/2.0f, y+h/2.0f);
    bd.angle = angle * b2_pi;
    wall->body = _world->CreateBody(&bd);
    // shape
    b2PolygonShape shape;
    shape.SetAsBox(w/2.0f, h/2.0f);
    // fixture
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.friction = 0.0f;
    wall->fixture = wall->body->CreateFixture(&fd);
    wall->type = type;
    wall->body->SetUserData(wall);
    return wall;
}

struct Bullet* World::createBullet(struct Player *player)
{
    int i = 1;
    if(!player->isOrientedRight) i = -1;
    b2Vec2 pos = b2Vec2(player->body->GetWorldCenter().x+i*2.2f,player->body->GetWorldCenter().y);
    struct Bullet *bullet = new struct Bullet;
    // body
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position = pos;
    bd.bullet = true;
    bullet->body = _world->CreateBody(&bd);
    // shape
    b2CircleShape shape;
    shape.m_radius = 0.1f;
    // fixture
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.restitution = 0.0f;
    fd.density = 0.01f;
    fd.isSensor = false;
    bullet->fixture = bullet->body->CreateFixture(&fd);
    bullet->type = Bullet;
    bullet->body->SetUserData(bullet);
    bullet->health=1;
    bullet->shooter=player;
    bullet->isOrientedRight=player->isOrientedRight;
    bullet->yPos=pos.y;
    return bullet;
}

struct Player* World::createPlayer(const b2Vec2& pos, const bool& isOrientedRight, const unsigned& ID)
{
    struct Player *player = new struct Player;
    // body
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.position = pos;
    player->body = _world->CreateBody(&bd);
    // shape
    int width = QApplication::desktop()->screenGeometry().size().width();
    int height = QApplication::desktop()->screenGeometry().size().height();
    b2PolygonShape shape;
    shape.SetAsBox(width/350.0f,height/135.0f);
    // fixture
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.friction = 1.0f;
    fd.density = 10000.0f;
    fd.restitution = 0.0f;
    player->fixture = player->body->CreateFixture(&fd);
    player->body->SetFixedRotation(true);
    player->type = Player;
    player->health = _health;
    player->consecJumps = 0;
    player->ID = ID;
    player->frame = 0;
    player->ammo = 3;
    player->isOrientedRight = isOrientedRight;
    player->isIdle = true;
    player->isProtected = false;
    player->isDead = false;
    player->isActive = false;
    player->isCharging = false;
    player->alreadyCharging = false;
    player->body->SetUserData(player);
    _objects.append(player);
    return player;
}

void World::paintEvent(QPaintEvent *)
{
    struct Player *deleteP = NULL;
    int width = QApplication::desktop()->screenGeometry().size().width();
    int height = QApplication::desktop()->screenGeometry().size().height();
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
	p.setTransform(_transform);
    foreach(struct Player* player, _players)
    {
        if(player==NULL) continue;
        if(!player->health)
        {
            player->isDead = true;
            deleteP = player;
            player->connection->hide();
            player->bullets->hide();
            player->ammoLabel->hide();
            QFont font;
            font.setPointSize(20);
            QLabel *dead = new QLabel("Dead");
            dead->setFont(font);
            _hboxPlayer[player->ID-1]->addWidget(dead);
            _hboxPlayer[player->ID-1]->addSpacerItem(new QSpacerItem(width/60.0f,0.0f));
        }
        if(player->isDead) continue;

        b2Vec2 vel = player->body->GetLinearVelocity();
        if(player->isIdle)
        {
            vel.x = 0;
            player->body->SetLinearVelocity(vel);
        }
        else
        {
            vel.x = width/44.0f;
            if(!player->isOrientedRight) vel.x *= -1;
            player->body->SetLinearVelocity(vel);
        }
        drawPlayer(&p, player);
        player->prevXpos = player->body->GetPosition().x;
    }
    foreach(Object* o, _objects)
    {
        if(o->type==Bullet)
        {
            struct Bullet * bullet = (struct Bullet *) o;
            if(!bullet->health)
            {
                _world->DestroyBody(o->body);
                _objects.removeOne(o);
                continue;
            }
            int i = 1;
            if(!bullet->isOrientedRight) i = -1;
            bullet->body->SetLinearVelocity(b2Vec2(i*width/17.0f,0.0f));
            o->body->SetTransform(b2Vec2(o->body->GetPosition().x, bullet->yPos), 0.0f);
            p.drawImage(QRectF(bullet->body->GetPosition().x-2.0f,bullet->body->GetPosition().y-0.75f,4.0f,1.5f),QImage(":/sprites/stuff/shotBullet.png").mirrored(bullet->isOrientedRight,false));
            //p.drawEllipse(QPointF(o->body->GetPosition().x, o->body->GetPosition().y), o->fixture->GetShape()->m_radius, o->fixture->GetShape()->m_radius); // Just for debugging purposes
        }
        //else drawWall(&p, o); // Just for debugging purposes
    }
    if(deleteP!=NULL)
    {
        _world->DestroyBody(deleteP->body);
        _players.removeOne(deleteP);
        _players.insert(deleteP->ID-1,NULL);
        unsigned aliveCounter = 0, alivePlayer = 0;
        foreach(struct Player* player, _players)
        {
            if(player!=NULL)
            {
                aliveCounter++;
                alivePlayer = player->ID;
            }
        }
        if(aliveCounter==1)
        {
            killTimer(_timerId);
            QDialog *finalPopUp = new QDialog;
            finalPopUp->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
            finalPopUp->setFixedSize(width/3.2,height/1.8);
            QVBoxLayout *mainLayout = new QVBoxLayout();
            finalPopUp->setLayout(mainLayout);
            QLabel *msg = new QLabel;
            QString playerLabel("Player Black");
            switch(alivePlayer)
            {
                case 2: playerLabel = QString("Player Blue"); break;
                case 3: playerLabel = QString("Player Red"); break;
                case 4: playerLabel = QString("Player Magenta"); break;
            }
            msg->setText(QString("Congratulations!\n\n").append(playerLabel).append(QString(" won the game!")));
            msg->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            QFont font;
            font.setPointSize(20);
            msg->setFont(font);
            mainLayout->addWidget(msg);
            mainLayout->addSpacerItem(new QSpacerItem(0.0f,height/100.0f));
            QPushButton *Ok = new QPushButton;
            Ok->setMaximumSize(finalPopUp->size().width()/3, finalPopUp->size().width()/6);
            Ok->setMinimumSize(finalPopUp->size().width()/3, finalPopUp->size().width()/6);
            Ok->setText(QString("Close"));
            Ok->setFont(font);
            mainLayout->setAlignment(Qt::AlignHCenter);
            QHBoxLayout *buttonLayout = new QHBoxLayout();
            buttonLayout->addWidget(Ok);
            buttonLayout->setAlignment(Qt::AlignHCenter);
            mainLayout->addLayout(buttonLayout);
            mainLayout->addSpacerItem(new QSpacerItem(0.0f,height/50.0f));
            QObject::connect(Ok,SIGNAL( clicked()),this,SLOT( endProgram() ) );
            finalPopUp->show();
        }
    }
}

void World::drawWall(QPainter *p, const Object *o)
{
    float32 x = o->body->GetPosition().x;
    float32 y = o->body->GetPosition().y;
    float32 angle = o->body->GetAngle();
    const b2PolygonShape *shape = dynamic_cast<b2PolygonShape*>(o->fixture->GetShape());
    float32 hx = shape->GetVertex(1).x;
    float32 hy = shape->GetVertex(2).y;
    QRectF r(x-hx, y-hy, 2*hx, 2*hy);
    p->save();
    p->translate(r.center());
    p->rotate(angle*180/b2_pi);
    p->translate(-r.center());
    p->drawRect(r);
    p->restore();
}

void World::drawPlayer(QPainter *p, struct Player *player)
{
    int width = QApplication::desktop()->screenGeometry().size().width();
    int height = QApplication::desktop()->screenGeometry().size().height();
    QString imgName(":/sprites/");
    float32 x = player->body->GetPosition().x;
    float32 y = player->body->GetPosition().y;
    player->frame++;
    if(x == player->prevXpos || player->isIdle)
    {
        imgName.append("idle/");
        player->frame %= 100;
        imgName.append(QString::number((player->ID * 10) +  player->frame/10));
    }
    else
    {
        imgName.append("run/");
        player->frame %= 60;
        imgName.append(QString::number((player->ID * 10) +  player->frame/10));
    }
    imgName.append(".png");
    p->drawImage(QRectF(x-width/192.0f,y-height/108.0f,width/96.0f,height/54.0f),QImage(imgName).mirrored(player->isOrientedRight,false));
    if(player->isProtected)
        p->drawImage(QRectF(x+width/250.0f,y-height/120.0f,width/767.0f,height/350.0f),QImage(":/sprites/stuff/shield.png"));
}

void World::start()
{
    if(!_timerId) _timerId = startTimer(1000/60); // 60fps
}

void World::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == _timerId)
    {
        _world->Step(1.0f/60.0f, 8, 3);
        update();
    }

}

void World::keyPressEvent(QKeyEvent *event)
{
    /* If the game is locked and we don't press the key to connect/disconnect, then close function */
    if((!_isReady && event->key() != Qt::Key_Space) || event->isAutoRepeat()) return;
    int i;
    switch(event->text().toInt())
    {
		case 1: ;
		case 2: ;
		case 3: ;
		case 4:
			i = event->text().toInt()-1;
			break;
			/* If it isn't a number from 1 to 4, then we know that the key press comes from the keyboard */
        default: // The key pressed comes from the keyboard
			/* If the game is controlled by an external controller, we exit the function */
			if(_keyboardPlayerID==0) return;
			char *out = (char *) malloc(3*sizeof(char));
			out[0] = _keyboardPlayerID+48; // Conversion from int to ASCII
            switch(event->key()) // We set out[1] to the corresponding key
			{ // Every key has a 'key' that allows us to recognize it during the intercommunication process
				case Qt::Key_Up:
					out[1] = '1';
					break;
				case Qt::Key_Down:
					out[1] = '2';
					break;
				case Qt::Key_Right:
					out[1] = '3';
					break;
				case Qt::Key_Left:
					out[1] = '4';
					break;
				case Qt::Key_Space:
					out[1] = '5';
					break;
				case Qt::Key_Escape:
					out[1] = '6';
					break;
				case Qt::Key_X:
					out[1] = '7';
					break;
				case Qt::Key_Z:
					out[1] = '8';
					break;
				default: // Key not recognized
					return;
			}

            out[2] = '1'; // We specify that the key is pressed (not released)
			_monitor.pressedKeys.enqueue(out);
			pthread_cond_signal(&_monitor.cond);
			return;
    }
    struct Player *player = _players.value(i);
    if(player==NULL) return;
    if(player->isDead) return;
    switch(event->key())
    {
    case Qt::Key_Up: // Recharging key
    {
        if(player->alreadyCharging || player->isProtected) return;
        pthread_t tid;
        pthread_create( &tid, NULL, recharge, (void *) player);
        break;

    }
    case Qt::Key_Down: // Protection key
    {
        player->isProtected = true;
        player->isIdle = true;
        break;
    }
    case Qt::Key_Right: // Right arrow key
    {
        if(player->isProtected) return;
        player->isIdle = false;
        player->isOrientedRight = true;
        break;
    }
    case Qt::Key_Left: // Left arrow key
    {
        if(player->isProtected) return;
        player->isIdle = false;
        player->isOrientedRight = false;
        break;
    }
    case Qt::Key_Z: // Shooting key
    {
        if(player->ammo <= 0 || player->isCharging || player->isProtected) return;
        player->ammo--;
        player->ammoLabel->setText(QString::number(player->ammo));
        struct Bullet *bullet = createBullet(player);
        _objects.append((Object *) bullet);
        int i = 1;
        if(!bullet->isOrientedRight) i = -1;
        bullet->body->SetLinearVelocity(b2Vec2(i*QApplication::desktop()->screenGeometry().width()/17.0f,0.0f));
        break;
    }
    case Qt::Key_X: // Jump key
    {
        if(player->consecJumps >= 2 || player->isProtected) return;
        b2Vec2 vel = player->body->GetLinearVelocity();
        vel.y = -QApplication::desktop()->screenGeometry().height()/12.8f;
        player->body->SetLinearVelocity(vel);
        player->consecJumps++;
        break;
    }
    case Qt::Key_Space: // Connect/Disconnect key
    {
        if(player->isActive)
        {
            _isReady = false;
            foreach(struct Player* p, _players)
            {
                if(p==NULL) continue;
                p->isActive = false;
                p->isProtected = false;
                p->isIdle = true;
                p->isCharging = false;
                p->connection->setPixmap(QPixmap(":/sprites/stuff/disconnected.png").scaled(1.0f,QApplication::desktop()->screenGeometry().height()/31.1f, Qt::KeepAspectRatioByExpanding));
            }
            foreach(Object* o, _objects)
            {
                if(o->type!=Bullet) continue;
                struct Bullet *bullet = (struct Bullet *) o;
                bullet->body->SetLinearVelocity(b2Vec2(0.0f,0.0f));
            }
        }
        else
        {
            player->isActive = true;
            player->connection->setPixmap(QPixmap(":/sprites/stuff/connected.png").scaled(1.0f,QApplication::desktop()->screenGeometry().height()/31.1f, Qt::KeepAspectRatioByExpanding));
            bool gameReady = true;
            foreach(struct Player* p, _players)
            {
                if(p==NULL) continue;
                if(p->isActive) continue;
                gameReady = false;
                break;
            }
            if(!gameReady) return;
            foreach(Object* o, _objects)
            {
                if(o->type!=Bullet) continue;
                struct Bullet *bullet = (struct Bullet *) o;
                int i = 1;
                if(!bullet->isOrientedRight) i = -1;
                bullet->body->SetLinearVelocity(b2Vec2(i*QApplication::desktop()->screenGeometry().width()/17.0f,0.0f));
            }
            _isReady = gameReady;
        }
        break;
    }
    case Qt::Key_Escape:
        break;
    }
}

void World::keyReleaseEvent(QKeyEvent *event)
{
    if(!_isReady || event->isAutoRepeat()) return;
    int i;
    switch(event->text().toInt())
    {
    case 1: ;
    case 2: ;
    case 3: ;
    case 4:
        i = event->text().toInt()-1;
        break;
        /* If it isn't a number from 1 to 4, then we know that the key press comes from the keyboard */
    default:
        /* If the game is controlled by an external controller, we exit the function */
        if(_keyboardPlayerID==0) return;
        char *out = (char *) malloc(3*sizeof(char));
        out[0] = _keyboardPlayerID+48; // Conversion from int to ASCII
        switch(event->key())
        {
        case Qt::Key_Up:
            out[1] = '1';
            break;
        case Qt::Key_Down:
            out[1] = '2';
            break;
        case Qt::Key_Right:
            out[1] = '3';
            break;
        case Qt::Key_Left:
            out[1] = '4';
            break;
        case Qt::Key_Space:
            out[1] = '5';
            break;
        case Qt::Key_Escape:
            out[1] = '6';
            break;
        case Qt::Key_X:
            out[1] = '7';
            break;
        case Qt::Key_Z:
            out[1] = '8';
            break;
        default: // Key not recognized
            return;
        }
        out[2] = '0'; // We specify that the key is released (not pressed)
        _monitor.pressedKeys.enqueue(out);
        pthread_cond_signal(&_monitor.cond);
        return;
    }
    struct Player *player = _players.value(i);
    if(player==NULL) return;
    if(player->isDead) return;
    switch(event->key())
    {
        case Qt::Key_Up: // Charging key
        {
            player->isCharging = false;
            break;
        }
        case Qt::Key_Down: // Protection key
        {
            player->isProtected = false;
            break;
        }
        case Qt::Key_Left: // Left arrow key
        {
            if(player->body->GetLinearVelocity().x<0) player->isIdle = true;
            break;
        }
        case Qt::Key_Right: // Right arrow key
        {
            if(player->body->GetLinearVelocity().x>0) player->isIdle = true;
            break;
        }
        case Qt::Key_X: // Jump key
        {
            if(player->body->GetLinearVelocity().y<0)
                player->body->SetLinearVelocity(b2Vec2(player->body->GetLinearVelocity().x,player->body->GetLinearVelocity().y/1.5));
            break;
        }
        default: // Key not recognized
            return;
    }
}

void World::endProgram() { exit(0); }

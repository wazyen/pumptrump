#-------------------------------------------------
#
# Project created by QtCreator 2018-04-14T15:31:18
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PumpTrump
TEMPLATE = app

SOURCES += main.cpp\
    World.cpp \
    ContactListener.cpp

HEADERS  += World.h \
    ContactListener.h \
    Declarations.h

FORMS    +=

INCLUDEPATH += ..

LIBS += -L"../Build/Box2D"

LIBS += -lBox2D

CONFIG += c++11

RESOURCES += \
    resource.qrc

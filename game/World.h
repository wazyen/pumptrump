#ifndef WORLD_H
#define WORLD_H

#include <Declarations.h>
#include <QtGui>
#include "Box2D/Box2D.h"
#include <QGLWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <unistd.h>
#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <QWidget>
#include <QKeyEvent>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QDialog>
#include <iostream>
#include <errno.h>

class ContactListener;

class World : public QWidget
{
    Q_OBJECT

public:
    World(char **argv);
    b2World* GetWorld();
    pthread_cond_t* getReady();
    void players(int nPlayers);
    void health(int health);
    Object* createWall(int type, float32 x, float32 y, float32 w, float32 h, float32 angle=0);
    struct Bullet* createBullet(struct Player *player);
    struct Player* createPlayer(const b2Vec2& pos, const bool& isOrientedRight, const unsigned& playerID);
    void paintEvent(QPaintEvent *);
    void drawWall(QPainter *p, const Object *o);
    void drawPlayer(QPainter *p, struct Player *o);
    void start();
    void timerEvent(QTimerEvent *event);

public slots:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void endProgram();

private:
    b2World *_world;
    QVector<Object*> _objects;
    QVector<struct Player*> _players;
    QHBoxLayout **_hboxPlayer;
    ContactListener* _contactListenerInstance;
    QTransform _transform;
    int _timerId, _keyboardPlayerID;
    unsigned _nPlayers, _health;
    bool _isReady;
    Monitor _monitor;
    pthread_cond_t _getReady;
    pthread_mutex_t _getReadyLock;
};

#endif

#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include "Box2D/Box2D.h"
#include <QVector>
#include <QQueue>
#include <QLabel>

class World;

enum
{
    Player,
    Floor,
    Bullet,
    WallObject,
};

struct Object
{
    int type;
    b2Body *body;
    b2Fixture *fixture;
};

struct Player : Object
{
    QVector<QLabel*> hearts;
    QLabel *connection, *bullets, *ammoLabel;
    float32 prevXpos;
    unsigned health, consecJumps, ID, frame, ammo;
    bool isOrientedRight, isIdle, isProtected, isDead, isActive, isCharging, alreadyCharging;
};

struct Bullet : Object
{

    struct Player *shooter;
    unsigned health;
    bool isOrientedRight;
    float32 yPos;
};

struct Monitor
{
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
    QQueue<char *> pressedKeys;
};

struct readServerParam
{
    World *world;
    char *readPort;
};

struct tellServerParam
{
    Monitor *monitor;
    char **argv;
};

#endif // DECLARATIONS_H

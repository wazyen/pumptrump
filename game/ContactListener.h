#ifndef CONTACTLISTENER_H
#define CONTACTLISTENER_H

#include "Declarations.h"
#include "Box2D/Box2D.h"
#include "World.h"

class ContactListener : public b2ContactListener
{
public:
    ContactListener() {}
    void BeginContact(b2Contact *contact);
};

#endif // CONTACTLISTENER_H

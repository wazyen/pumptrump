#include "World.h"

int main(int argc, char** argv)
{
    if(argv[2][0]=='0')
    {
        if (argc != 3)
        {
            qDebug() << "Usage: <programpath> <broadcastPort> <keyboardPlayerID>";
            exit(1);
        }
    }
    else
    {
        if (argc != 5)
        {
            qDebug() << "Usage: <programpath> <broadcastPort> <keyboardPlayerID> <serverIp> <serverPort>";
            exit(1);
        }
    }
    QApplication app(argc, argv);
    World world(argv);
    QPalette palette;
    QRect rec = QApplication::desktop()->screenGeometry();
    palette.setBrush(world.backgroundRole(), QBrush(QImage(":/sprites/stuff/whitehouse.png").scaled(QSize(rec.width(),rec.height()))));
    world.setPalette(palette);
    world.window()->setWindowIcon(QIcon(QString(":/sprites/stuff/icon.ico")));
    world.showFullScreen();
    world.show();
    world.start();

    return app.exec();
}

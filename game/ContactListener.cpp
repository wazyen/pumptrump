#include "ContactListener.h"
#include <QDebug>

void ContactListener::BeginContact(b2Contact* contact)
{
    b2Body *bodyA = contact->GetFixtureA()->GetBody();
    b2Body *bodyB = contact->GetFixtureB()->GetBody();
    struct Object* bodyUserData1 = (struct Object *) bodyA->GetUserData();
    struct Object* bodyUserData2 = (struct Object *) bodyB->GetUserData();

    if(bodyUserData1->type==Player && (bodyUserData2->type==Floor || bodyUserData2->type==Player) && bodyA->GetWorldCenter().y<bodyB->GetWorldCenter().y)
    {
        b2PolygonShape *shapeA = (b2PolygonShape *) bodyA->GetFixtureList()->GetShape();
        b2PolygonShape *shapeB = (b2PolygonShape *) bodyB->GetFixtureList()->GetShape();
        struct Player *player = (struct Player *) bodyUserData1;
        if((bodyA->GetPosition().x-shapeA->GetVertex(1).x/2)<(bodyB->GetPosition().x+shapeB->GetVertex(1).x) && (bodyA->GetPosition().x+shapeA->GetVertex(1).x)>(bodyB->GetPosition().x-shapeB->GetVertex(1).x))
            player->consecJumps = 0;
    }
    if(bodyUserData2->type==Player && (bodyUserData1->type==Floor || bodyUserData1->type==Player) && bodyB->GetWorldCenter().y<bodyA->GetWorldCenter().y)
    {
        b2PolygonShape *shapeA = (b2PolygonShape *) bodyA->GetFixtureList()->GetShape();
        b2PolygonShape *shapeB = (b2PolygonShape *) bodyB->GetFixtureList()->GetShape();
        struct Player *player = (struct Player *) bodyUserData2;
        if((bodyB->GetPosition().x-shapeB->GetVertex(1).x/2)<(bodyA->GetPosition().x+shapeA->GetVertex(1).x) && (bodyB->GetPosition().x+shapeB->GetVertex(1).x)>(bodyA->GetPosition().x-shapeA->GetVertex(1).x))
            player->consecJumps = 0;
    }

    if(bodyA->IsBullet())
    {
        struct Bullet *bullet = (struct Bullet *) bodyUserData1;
        if(bullet->shooter->body!=bodyB)
        {
            bullet->health--;
            struct Player *player = (struct Player *) bodyUserData2;
            if(player->type==Player && !player->isProtected)
                player->hearts.value(--player->health)->hide();
        }
    }
    if(bodyB->IsBullet())
    {
        struct Bullet *bullet = (struct Bullet *) bodyUserData2;
        if(bullet->shooter->body!=bodyA)
        {
            bullet->health--;
            struct Player *player = (struct Player *) bodyUserData1;
            if(player->type==Player && !player->isProtected)
                player->hearts.value(--player->health)->hide();
        }
    }
  }
